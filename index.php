<style type="text/css">
	* {
		font-family: arial;
	}

	ul {
		list-style-type: none;
		width: 500px;
	}

	li {
		height: 40px;
		line-height: 40px;
		background-color: lightblue;
		margin: 10px 0;
		padding: 0 0 0 10px;
	}


	li:hover {
		background-color: blue;
	}

</style>

<?php

include 'functions.php';


$lines = getFileContent('vot.dat');

$votes = parseVotes($lines);

$mysqli = dbConnect('localhost', 'root', 'testing1', 'tw-ex151');

insertIntoDB($mysqli, $votes);


$category_votes = getVotesPerCategory($mysqli);

$total_votes = totalVotesCounter($category_votes);



showChart($category_votes, $total_votes);