<?php


function totalVotesCounter($categ_votes) {

	$sum = 0;
	foreach ($categ_votes as $categ) {
		$sum += $categ['no_votes'];
	}

	return $sum;
}

function showChart($categ_votes, $total_votes) {


	echo "<ul>";

	foreach ($categ_votes as $categ) {
		showCategVotes($categ['categ'], $categ['no_votes'], $total_votes);

	}

	echo "</ul>";


}

function showCategVotes($categ, $no_votes, $total_votes) {

	$width = $no_votes * 100 / $total_votes;
	$style = "width: {$width}%";
	echo "<li style=\"{$style}\">{$categ}</li>";
}



function getVotesPerCategory($mysqli) {

	$sql = 'SELECT categ, count(*) as no_votes FROM `votes` group by categ';

	$result = $mysqli->query($sql);

	$categories = [];

	while ( $row = $result->fetch_assoc() ) {
		$categories[]  = $row;
	}

	return $categories;
}



function dbConnect($host, $user, $pass, $db) {

	$mysqli = new mysqli($host, $user, $pass, $db);

	if ($mysqli->connect_error) {
	    die('Connect Error (' . $mysqli->connect_errno . ') '
	            . $mysqli->connect_error);
	}

	return $mysqli;
}


function insertIntoDB($mysqli, $votes) {

	foreach ($votes as $vote) {
		$date = DateTime::createFromFormat('Y M d', '2015 ' . $vote['date']);
		$dateMysql =  $date->format('Y-m-d');//2015-04-30
		$sql = "INSERT INTO votes SET `date`='{$dateMysql}', name='{$vote['name']}', categ='{$vote['categ']}'";

		$mysqli->query($sql);
	}
}






function parseVotes($lines) {

	$votes = [];
	foreach ($lines as $line) {
    	$fields = unpack('A7number/A7date/A36name/A12dummy/A2categ', $line);

    	$votes[] = array(
    			'date' => trim($fields['date']),
    			'name' => trim($fields['name']),
    			'categ' => trim($fields['categ']),
    		);
	}

	return $votes;
}


function getFileContent($filename) {
	$content = file_get_contents($filename);

	$lines = explode("\n", $content);

	return $lines;
}